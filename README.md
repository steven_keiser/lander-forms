# Lander Forms
This package contains assets that are used on all lander pages under the
**_`depot.som.yale.edu`_** subdomain.

## Table of Contents
- Installation
- React Library
- PHP Scripts
- Slate Configuration

## Installation
In the lander's project folder, navigate to the where the `package.json` file is
location run the following:

For NPM
```bash
npm install --save https://github.com/yalesom/lander-forms.git
```
For Yarn
```bash
yarn add https://github.com/yalesom/lander-forms.git
```
## React Library
The **react-lander-forms** folder contains

## PHP Scripts
When this package is installed it will copy the contents of the **php-scripts**
into a **php** folder in the project's local folder. These local copies should
**_NOT_** be modified in any way, for any reason.

_`azure-search.php`_\
This file uses the Microsoft Azure API to perform geolocation. It contains
functions that are called via JavaScript to find a user either by latitude
and longitude (reverse geocode) or zip code lookup (address geocode).

_`slate-processing.php`_\
This file translates the `slate.config.json` file (see below) so data from a
the local form can be posted into Slate.

## Slate Configuration
Create a named **`slate.config.json`** file into root directory of your
project's local folder and copy the following:

```json
{
  "form_id": "",
  "domain": "",
  "field_mappings": {
    "example_input": "form_2698f238-8bc8-479a-8912-c5f69af92430"
  }
}
```
To get the information for the configuration file, go to published Slate form's
url in your browser and run the following code in the console:

```Javascript
jQuery("form").each(function(){console.info(`form_id | ${$(this).attr("id").split("_")[1]}`);console.info(`domain | ${window.location.origin}`);console.info("field_mappings");$(this).find("label").each(function(){console.info(`${$(this).text()} | ${$(this).attr("for")}`)})});
```

Enter the values for both `form_id` and `domain` that are displayed.

The `field_mappings` are defined as **_key_**\\**_value_** pairs. The **_key_**
is the input name from the local lander form. Using the label shown from the
Slate form, find the one that closely matches the input name from the local
form and use that hash for the **_value_**.
