import { IAddress } from '../interfaces';

interface ILocationField {
  fieldName: string;
  htmlElement: any;
  formElement: any;
}

export const getLocationFields = (): ILocationField[] => {
  let city = document.querySelector('.form__element--city');
  let state = document.querySelector('.form__element--state');
  let zipCode = document.querySelector('.form__element--zip-code');
  let country = document.querySelector('.form__element--country');

  return [
    {
      fieldName: 'city',
      htmlElement: city,
      formElement: (city) ? city.firstChild : undefined
    },
    {
      fieldName: 'state',
      htmlElement: state,
      formElement: (state) ? state.firstChild : undefined
    },
    {
      fieldName: 'zip_code',
      htmlElement: zipCode,
      formElement: (zipCode) ? zipCode.firstChild : undefined
    },
    {
      fieldName: 'country',
      htmlElement: country,
      formElement: (country) ? country.firstChild : undefined
    }
  ];
};

/**
 * Makes the all location fields visible.
 */
export const showLocationFields = (): void => {
  let locationFields: ILocationField[] = getLocationFields();

  locationFields.forEach(field => {
    if (field) {
      field.htmlElement.classList.remove('form__element--hidden-field');
    }
  });
};

/**
 * Makes the all location fields visible.
 */
export const hideLocationFields = (): void => {
  let city = document.querySelector('.form__element--city');
  let state = document.querySelector('.form__element--state');
  let zipCode = document.querySelector('.form__element--zip-code');
  let country = document.querySelector('.form__element--country');

  let locationFields = [
      city,
      state,
      zipCode,
      country
  ];

  locationFields.forEach(field => {
    if (field) {
      field.classList.add('form__element--hidden-field');
    }
  })
};

/**
 * Update the location fields using the address param.
 *
 * @param {IAddress} address
 *   The object containing the address information.
 */
export const populateLocationFields = (address: IAddress): void => {
  let locationFields: ILocationField[] = getLocationFields();

  locationFields.forEach((field: any) => {
    if (field) {
      if (field.formElement) {
        field.formElement.value = address[field.fieldName];
      }
    }
  });
};

/**
 * Removes all the values from the location fields.
 */
export const clearLocationFields = (): void => {
  let locationFields: ILocationField[] = getLocationFields();

  locationFields.forEach((field: any) => {
    if (field) {
      if (field.formElement) {
        field.formElement.value = '';
      }
    }
  });
}
