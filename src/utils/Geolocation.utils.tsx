import { showAddressListField, updateAddressListDropdown } from "./AddressListField.utils";
import { clearLocationFields, hideLocationFields, populateLocationFields, showLocationFields } from "./LocationFields.utils";

/**
 * Uses the browser's geolocation, if allowed, to find the users location.
 */
export const findMe = (resourceUrl: string) => {
  const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      async (position: any) => {
        let formData = new FormData();

        formData.append('latitude', position.coords.latitude);
        formData.append('longitude', position.coords.longitude);
        formData.append('action', 'get_address_by_geolocation');

        let result = await (await fetch(resourceUrl, {
          method: 'post',
          body: formData
        })).json();

        // Good response from Azure.
        if (result.statusCode === 200) {
          hideFindMe();
          populateLocationFields(result.data.addresses[0]);
        }

        // Bad response from Azure.
        else {
          hideFindMe();

          if (FORM_ERROR_MESSAGE) {
            FORM_ERROR_MESSAGE.innerHTML = result.data.error;
          }
        }

        showLocationFields();
      },

      // Error processing geolocation.
      (error: any) => {
        const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');

        if (FORM_ERROR_MESSAGE) {
          switch(error.code) {
            case error.PERMISSION_DENIED:
              FORM_ERROR_MESSAGE.innerHTML = 'Request for Geolocation denied, please enter your location information.';
              break;
            case error.POSITION_UNAVAILABLE:
              FORM_ERROR_MESSAGE.innerHTML = 'Location information is unavailable, please enter your location information.'
              break;
            default:
              FORM_ERROR_MESSAGE.innerHTML = 'An unknown error occurred, please enter your location information.'
          }
        }

        hideFindMe();
        showLocationFields();
      }
    );
  }

  // Geolocation is not available.
  else {
    hideFindMe();
    showLocationFields();

    if (FORM_ERROR_MESSAGE) {
      FORM_ERROR_MESSAGE.innerHTML = 'There was an issue with geolocation, please enter your location information.';
    }
  }
};

export const handleZipCodeChange = (evt: any, resourceUrl: string, minLength: number = 5) => {
  const { value } = evt.target as HTMLSelectElement;
  const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');

  if (value.length >= minLength) {
    setTimeout(async () => {
      let formData = new FormData();

      formData.append('zip_code', value);
      formData.append('action', 'get_address_by_zip_code');

      let result = await (await fetch(resourceUrl, {
        method: 'post',
        body: formData
      })).json();

      // Good response from Azure.
      if (result.statusCode === 200) {
        updateAddressListDropdown(result.data.addresses);
        showAddressListField();
        hideLocationFields();
        clearLocationFields();
      }

      // Bad response from Azure.
      else {
        showLocationFields();

        if (FORM_ERROR_MESSAGE) {
          FORM_ERROR_MESSAGE.innerHTML = result.data.error;
        }
      }
    }, 300)
  }
};

/**
 * Hides the find me button.
 */
export const hideFindMe = (): void => {
  let findMe = document.querySelector('.form__element--find-me');

  if (findMe) {
    findMe.classList.add('form__element--hidden-field');
  }
};
