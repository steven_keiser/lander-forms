import { IAddress } from "../interfaces";
import { populateLocationFields, showLocationFields } from "./LocationFields.utils";

/**
 * Shows the address list field.
 */
export const showAddressListField = (): void => {
  let addressList = document.querySelector('.form__element--address-list');

  if (addressList) {
    addressList.classList.remove('form__element--hidden-field');
  }
};

/**
 * Hides the address list field.
 */
export const hideAddressListField = (): void => {
  let addressList = document.querySelector('.form__element--address-list');

  if (addressList) {
    addressList.classList.add('form__element--hidden-field');
  }
};

/**
 * Populates a select list with addresses returned from Azure.
 *
 * @param {IAddress[]} addresses
 *   An array of address objects.
 */
export const updateAddressListDropdown = (addresses?: IAddress[]): void => {
  if (addresses) {
    let addresslistSelect = document.querySelector('.form__element--address-list select') as HTMLSelectElement;
    let placeholderOption = document.createElement('option');
    let notaOption = document.createElement('option');

    // Remove all existing options.
    addresslistSelect.options.length = 0;

    // Add a placeholder option.
    placeholderOption.setAttribute('value', '');
    placeholderOption.text = 'Select an address';
    addresslistSelect.add(placeholderOption);

    // Loop through all addresses and create an option for each.
    addresses.forEach((address: IAddress) => {
      let addressOption = document.createElement('option');

      addressOption.setAttribute('value', JSON.stringify(address));
      addressOption.text = `${ address.city }, ${ address.state }, ${ address.zip_code }, ${ address.country }`;

      if (addresslistSelect) {
        addresslistSelect.add(addressOption);
      }
    });

    // Add a "none of the above" option.
    notaOption.setAttribute('value', '__NONE__');
    notaOption.text = 'None of the above';
    addresslistSelect.add(notaOption);
  }
};

export const handleAddressListChange = (evt: any) => {
  const { value } = evt.target as HTMLSelectElement;

  if (value !== '__NONE__' && value !== '') {
    populateLocationFields(JSON.parse(value));
  }

  hideAddressListField();
  showLocationFields();
};
