interface IFormProps {
  geolocationUrl: string;
  formProcessUrl: string;
}

export default IFormProps;
