<?php

/**
 * @file
 * slate-processing.php
 */

header('content-type: application/json');

// Get the lander configuration file.
$slate_config = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/slate.config.json');

// Convert it into a PHP variable.
$slate_config = json_decode($slate_config, TRUE);

// Get all the slate config options.
$slate_form_id = $slate_config['form_id'];
$slate_domain = $slate_config['domain'];
$slate_field_mappings = $slate_config['field_mappings'];

$response = [];

if (empty($slate_domain) || empty($slate_form_id) || empty($slate_field_mappings)) {
  $response['statusCode'] = 400;
}
else {
  // Add the form id and cmd to complete the slate url.
  $slate_url = $slate_domain . '/register/?id=' . $slate_form_id . '&cmd=api&';

  // Create an array of slate fields using the $slate_field for the key and
  // $form_field to get the $_POST data.
  $slate_fields = [];

  foreach ($slate_field_mappings as $form_field => $slate_field) {
    $slate_fields[$slate_field] = isset($_POST[$form_field]) ? trim($_POST[$form_field]) : '';
  }

  // Initialize the curl connection.
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_URL, $slate_url);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($slate_fields));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

  $ch_response = curl_exec($ch);
  $ch_status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

  $response['statusCode'] = 200;

  curl_close($ch);
}

print json_encode($response);
