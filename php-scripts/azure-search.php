<?php

/**
 * @file
 * azure-search.php
 */

define('AZURE_KEY', 'wZi5-1bNrmqV1wdCJgkTygIfJZP8RL9MtBPgziGWqbE');
define('AZURE_API_VERSION', '1.0');

if (!isset($_POST['action'])) {
  $response = [];
  $response['statusCode'] = 404;
  $response['data'] = [];
  $response['data']['error'] = 'No function found with name ' . $_POST['action'];
}

$function = $_POST['action'];

if (function_exists($function)) {
  $function();
}

/**
 * Reverse Geocode to an address using Lat/Lon.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/maps/search/getsearchaddressreverse
 */
function get_address_by_geolocation() {
  header('content-type: application/json');

  $ch = curl_init();

  $url = 'https://atlas.microsoft.com/search/address/reverse/json';
  $latitude = $_POST['latitude'];
  $longitude = $_POST['longitude'];

  $url .= '?subscription-key=' . AZURE_KEY;
  $url .= '&api-version=' . AZURE_API_VERSION;
  $url .= '&query=' . $latitude . ',' . $longitude;

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

  $ch_response = json_decode(curl_exec($ch));
  $ch_status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

  $response = [];
  $response['data'] = [];

  // Response from Azure.
  if ($ch_status == 200) {
    $response['statusCode'] = 200;

    $addresses = [];

    foreach ($ch_response->addresses as $address) {
      $address = $address->address;

      $addresses[] = [
        'city' => isset($address->municipality) ? $address->municipality : NULL,
        'state' => isset($address->countrySubdivisionName) ? $address->countrySubdivisionName : NULL,
        'state_abbreviation' => isset($address->countrySubdivision) ? $address->countrySubdivision : NULL,
        'zip_code' => isset($address->postalCode) ? $address->postalCode : NULL,
        'country' => isset($address->country) ? $address->country : NULL,
        'country_code' => isset($address->countryCode) ? $address->countryCode : NULL,
      ];
    }

    $response['data']['addresses'] = $addresses;
  }

  // Bad response from Azure.
  else {
    $response['statusCode'] = 400;

    $response['data']['error'] = 'Unable to get a reliable geolocation, please enter your location information.';
  }

  print json_encode($response);

  curl_close($ch);
}

/**
 * Address Geocoding using zip code only.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/maps/search/getsearchaddress
 */
function get_address_by_zip_code() {
  header('content-type: application/json');

  $ch = curl_init();

  $url = 'https://atlas.microsoft.com/search/address/json';
  $zip_code = $_POST['zip_code'];

  $url .= '?subscription-key=' . AZURE_KEY;
  $url .= '&api-version=' . AZURE_API_VERSION;
  $url .= '&query=' . $zip_code;
  $url .= '&limit=100';

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

  $ch_response = json_decode(curl_exec($ch));
  $ch_status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

  $response = [];
  $response['data'] = [];

  // Response from Azure.
  if ($ch_status == 200 && count($ch_response->results) > 0) {
    $response['statusCode'] = 200;

    $addresses = [];

    foreach ($ch_response->results as $result) {
      $address = $result->address;

      if (isset($address->municipality) && isset($address->countrySubdivisionName)) {
        foreach (explode(',', $address->municipality) as $city) {
          $addresses[] = [
            'city' => trim($city),
            'state' => $address->countrySubdivisionName,
            'state_abbreviation' => ($address->countrySubdivision) ? $address->countrySubdivision : NULL,
            'zip_code' => $zip_code,
            'country' => isset($address->country) ? $address->country : NULL,
            'country_code' => isset($address->countryCode) ? $address->countryCode : NULL,
          ];
        }
      }
    }

    $addresses = array_unique($addresses, SORT_REGULAR);
    $response['data']['addresses'] = $addresses;
  }

  // Bad response from Azure.
  else {
    $response['statusCode'] = 400;

    $response['data']['error'] = 'Unable to get a location from your zip code, please complete your location information.';
  }

  print json_encode($response);

  curl_close($ch);
}
