interface IAddress {
    [key: string]: any;
    city: string | null;
    state: string | null;
    state_abbreviation: string | null;
    zip_code: string | null;
    country: string | null;
    country_code: string | null;
}
export default IAddress;
