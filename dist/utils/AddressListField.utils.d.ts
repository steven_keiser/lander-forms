import { IAddress } from "../interfaces";
/**
 * Shows the address list field.
 */
export declare const showAddressListField: () => void;
/**
 * Hides the address list field.
 */
export declare const hideAddressListField: () => void;
/**
 * Populates a select list with addresses returned from Azure.
 *
 * @param {IAddress[]} addresses
 *   An array of address objects.
 */
export declare const updateAddressListDropdown: (addresses?: IAddress[] | undefined) => void;
export declare const handleAddressListChange: (evt: any) => void;
