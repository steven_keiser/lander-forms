"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleAddressListChange = exports.updateAddressListDropdown = exports.hideAddressListField = exports.showAddressListField = void 0;
const LocationFields_utils_1 = require("./LocationFields.utils");
/**
 * Shows the address list field.
 */
const showAddressListField = () => {
    let addressList = document.querySelector('.form__element--address-list');
    if (addressList) {
        addressList.classList.remove('form__element--hidden-field');
    }
};
exports.showAddressListField = showAddressListField;
/**
 * Hides the address list field.
 */
const hideAddressListField = () => {
    let addressList = document.querySelector('.form__element--address-list');
    if (addressList) {
        addressList.classList.add('form__element--hidden-field');
    }
};
exports.hideAddressListField = hideAddressListField;
/**
 * Populates a select list with addresses returned from Azure.
 *
 * @param {IAddress[]} addresses
 *   An array of address objects.
 */
const updateAddressListDropdown = (addresses) => {
    if (addresses) {
        let addresslistSelect = document.querySelector('.form__element--address-list select');
        let placeholderOption = document.createElement('option');
        let notaOption = document.createElement('option');
        // Remove all existing options.
        addresslistSelect.options.length = 0;
        // Add a placeholder option.
        placeholderOption.setAttribute('value', '');
        placeholderOption.text = 'Select an address';
        addresslistSelect.add(placeholderOption);
        // Loop through all addresses and create an option for each.
        addresses.forEach((address) => {
            let addressOption = document.createElement('option');
            addressOption.setAttribute('value', JSON.stringify(address));
            addressOption.text = `${address.city}, ${address.state}, ${address.zip_code}, ${address.country}`;
            if (addresslistSelect) {
                addresslistSelect.add(addressOption);
            }
        });
        // Add a "none of the above" option.
        notaOption.setAttribute('value', '__NONE__');
        notaOption.text = 'None of the above';
        addresslistSelect.add(notaOption);
    }
};
exports.updateAddressListDropdown = updateAddressListDropdown;
const handleAddressListChange = (evt) => {
    const { value } = evt.target;
    if (value !== '__NONE__' && value !== '') {
        LocationFields_utils_1.populateLocationFields(JSON.parse(value));
    }
    exports.hideAddressListField();
    LocationFields_utils_1.showLocationFields();
};
exports.handleAddressListChange = handleAddressListChange;
//# sourceMappingURL=AddressListField.utils.js.map