/**
 * Uses the browser's geolocation, if allowed, to find the users location.
 */
export declare const findMe: (resourceUrl: string) => void;
export declare const handleZipCodeChange: (evt: any, resourceUrl: string, minLength?: number) => void;
/**
 * Hides the find me button.
 */
export declare const hideFindMe: () => void;
