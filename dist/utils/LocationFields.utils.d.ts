import { IAddress } from '../interfaces';
interface ILocationField {
    fieldName: string;
    htmlElement: any;
    formElement: any;
}
export declare const getLocationFields: () => ILocationField[];
/**
 * Makes the all location fields visible.
 */
export declare const showLocationFields: () => void;
/**
 * Makes the all location fields visible.
 */
export declare const hideLocationFields: () => void;
/**
 * Update the location fields using the address param.
 *
 * @param {IAddress} address
 *   The object containing the address information.
 */
export declare const populateLocationFields: (address: IAddress) => void;
/**
 * Removes all the values from the location fields.
 */
export declare const clearLocationFields: () => void;
export {};
