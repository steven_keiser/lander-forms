"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.clearLocationFields = exports.populateLocationFields = exports.hideLocationFields = exports.showLocationFields = exports.getLocationFields = void 0;
const getLocationFields = () => {
    let city = document.querySelector('.form__element--city');
    let state = document.querySelector('.form__element--state');
    let zipCode = document.querySelector('.form__element--zip-code');
    let country = document.querySelector('.form__element--country');
    return [
        {
            fieldName: 'city',
            htmlElement: city,
            formElement: (city) ? city.firstChild : undefined
        },
        {
            fieldName: 'state',
            htmlElement: state,
            formElement: (state) ? state.firstChild : undefined
        },
        {
            fieldName: 'zip_code',
            htmlElement: zipCode,
            formElement: (zipCode) ? zipCode.firstChild : undefined
        },
        {
            fieldName: 'country',
            htmlElement: country,
            formElement: (country) ? country.firstChild : undefined
        }
    ];
};
exports.getLocationFields = getLocationFields;
/**
 * Makes the all location fields visible.
 */
const showLocationFields = () => {
    let locationFields = exports.getLocationFields();
    locationFields.forEach(field => {
        if (field) {
            field.htmlElement.classList.remove('form__element--hidden-field');
        }
    });
};
exports.showLocationFields = showLocationFields;
/**
 * Makes the all location fields visible.
 */
const hideLocationFields = () => {
    let city = document.querySelector('.form__element--city');
    let state = document.querySelector('.form__element--state');
    let zipCode = document.querySelector('.form__element--zip-code');
    let country = document.querySelector('.form__element--country');
    let locationFields = [
        city,
        state,
        zipCode,
        country
    ];
    locationFields.forEach(field => {
        if (field) {
            field.classList.add('form__element--hidden-field');
        }
    });
};
exports.hideLocationFields = hideLocationFields;
/**
 * Update the location fields using the address param.
 *
 * @param {IAddress} address
 *   The object containing the address information.
 */
const populateLocationFields = (address) => {
    let locationFields = exports.getLocationFields();
    locationFields.forEach((field) => {
        if (field) {
            if (field.formElement) {
                field.formElement.value = address[field.fieldName];
            }
        }
    });
};
exports.populateLocationFields = populateLocationFields;
/**
 * Removes all the values from the location fields.
 */
const clearLocationFields = () => {
    let locationFields = exports.getLocationFields();
    locationFields.forEach((field) => {
        if (field) {
            if (field.formElement) {
                field.formElement.value = '';
            }
        }
    });
};
exports.clearLocationFields = clearLocationFields;
//# sourceMappingURL=LocationFields.utils.js.map