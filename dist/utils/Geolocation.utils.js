"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.hideFindMe = exports.handleZipCodeChange = exports.findMe = void 0;
const AddressListField_utils_1 = require("./AddressListField.utils");
const LocationFields_utils_1 = require("./LocationFields.utils");
/**
 * Uses the browser's geolocation, if allowed, to find the users location.
 */
const findMe = (resourceUrl) => {
    const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => __awaiter(void 0, void 0, void 0, function* () {
            let formData = new FormData();
            formData.append('latitude', position.coords.latitude);
            formData.append('longitude', position.coords.longitude);
            formData.append('action', 'get_address_by_geolocation');
            let result = yield (yield fetch(resourceUrl, {
                method: 'post',
                body: formData
            })).json();
            // Good response from Azure.
            if (result.statusCode === 200) {
                exports.hideFindMe();
                LocationFields_utils_1.populateLocationFields(result.data.addresses[0]);
            }
            // Bad response from Azure.
            else {
                exports.hideFindMe();
                if (FORM_ERROR_MESSAGE) {
                    FORM_ERROR_MESSAGE.innerHTML = result.data.error;
                }
            }
            LocationFields_utils_1.showLocationFields();
        }), 
        // Error processing geolocation.
        (error) => {
            const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');
            if (FORM_ERROR_MESSAGE) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        FORM_ERROR_MESSAGE.innerHTML = 'Request for Geolocation denied, please enter your location information.';
                        break;
                    case error.POSITION_UNAVAILABLE:
                        FORM_ERROR_MESSAGE.innerHTML = 'Location information is unavailable, please enter your location information.';
                        break;
                    default:
                        FORM_ERROR_MESSAGE.innerHTML = 'An unknown error occurred, please enter your location information.';
                }
            }
            exports.hideFindMe();
            LocationFields_utils_1.showLocationFields();
        });
    }
    // Geolocation is not available.
    else {
        exports.hideFindMe();
        LocationFields_utils_1.showLocationFields();
        if (FORM_ERROR_MESSAGE) {
            FORM_ERROR_MESSAGE.innerHTML = 'There was an issue with geolocation, please enter your location information.';
        }
    }
};
exports.findMe = findMe;
const handleZipCodeChange = (evt, resourceUrl, minLength = 5) => {
    const { value } = evt.target;
    const FORM_ERROR_MESSAGE = document.querySelector('.form__error-message');
    if (value.length >= minLength) {
        setTimeout(() => __awaiter(void 0, void 0, void 0, function* () {
            let formData = new FormData();
            formData.append('zip_code', value);
            formData.append('action', 'get_address_by_zip_code');
            let result = yield (yield fetch(resourceUrl, {
                method: 'post',
                body: formData
            })).json();
            // Good response from Azure.
            if (result.statusCode === 200) {
                AddressListField_utils_1.updateAddressListDropdown(result.data.addresses);
                AddressListField_utils_1.showAddressListField();
                LocationFields_utils_1.hideLocationFields();
                LocationFields_utils_1.clearLocationFields();
            }
            // Bad response from Azure.
            else {
                LocationFields_utils_1.showLocationFields();
                if (FORM_ERROR_MESSAGE) {
                    FORM_ERROR_MESSAGE.innerHTML = result.data.error;
                }
            }
        }), 300);
    }
};
exports.handleZipCodeChange = handleZipCodeChange;
/**
 * Hides the find me button.
 */
const hideFindMe = () => {
    let findMe = document.querySelector('.form__element--find-me');
    if (findMe) {
        findMe.classList.add('form__element--hidden-field');
    }
};
exports.hideFindMe = hideFindMe;
//# sourceMappingURL=Geolocation.utils.js.map